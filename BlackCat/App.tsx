import React from 'react';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'mobx-react';
import Router from './android/app/src/navigator/router';
import store from './android/app/src/stores';

const AppContainer = createAppContainer(Router);

const App = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
);

export default App;
