import { createStackNavigator } from 'react-navigation-stack';
import Login from '../pages/login';
import Main from '../pages/main';
import Me from '../pages/me';
import Search from '../pages/search';

const Router = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      headerShown: false, // 去头部
    }),
  },
  Main: {
    // 主页面
    screen: Main, // tab导航配置
    navigationOptions: ({ navigation }) => ({
      headerShown: false, // 去头部
    }),
  },
  Me: {
    screen: Me,
    navigationOptions: ({ navigation }) => ({
      headerShown: false, // 去头部
    }),
  },
  Search: {
    screen: Search,
    navigationOptions: ({ navigation }) => ({
      headerShown: false, // 去头部
    }),
  },
  initialRouteName: 'Login', // 默认登录页
  headerMode: 'screen',
});

export default Router;
