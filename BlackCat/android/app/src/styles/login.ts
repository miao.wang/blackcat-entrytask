import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    alignItems: 'center',
  },
  main: {
    flex: 1,
    width: '100%',
    paddingTop: 70,
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center',
    backgroundColor: 'rgba(133, 96, 169, 0.7)',
  },
  slogan: {
    height: 20,
    lineHeight: 20,
  },
  signBtn: {
    height: 64,
    backgroundColor: '#D5EF7F',
  },
  btnText: {
    fontSize: 16,
    color: '#453257',
    textAlign: 'center',
    lineHeight: 64,
    fontWeight: '700',
  },
  BC: {
    height: 31,
    lineHeight: 31,
    marginTop: 16,
    color: '#D5EF7F',
  },
  icon: {
    height: 64,
    width: 64,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 37,
    borderRadius: 32,
    borderColor: '#D5EF7F',
    borderWidth: 1,
  },
  cat: {
    height: 64,
    width: 64,
    backgroundColor: 'green',
  },
  input: {
    height: 40,
    width: 240,
    // backgroundColor: '#D3C1E5',
    borderWidth: 1,
    borderColor: '#FFFFFF',
    borderRadius: 20,
  },
  username: {
    marginTop: 118,
  },
  password: {
    marginTop: 16,
  },
});
