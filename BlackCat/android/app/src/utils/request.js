const baseUrl = 'http://localhost:3000/';
const signIn = async userInfo => {
  try {
    const res = await fetch(`${baseUrl}auth/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userInfo),
    });
    return res.json();
  } catch (e) {
    throw e;
  }
};

const getItems = async () => {
  try {
    const res = await fetch(`${baseUrl}events`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return res.json();
  } catch (e) {
    throw e;
  }
};

export { signIn, getItems };
