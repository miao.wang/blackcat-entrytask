import { runInAction } from 'mobx';
import * as Types from '../typings/user';
import { signIn, getItems } from '../utils/request';
import AsyncStorage from '@react-native-async-storage/async-storage';

class Store {
  userInfo: Partial<Types.UserInfo> = {};
  events?: Types.Events;

  async logIn(postData: Types.PostData) {
    try {
      const info: Types.UserInfo = await signIn(postData);
      runInAction(() => {
        this.userInfo = info;
      });
      await AsyncStorage.setItem('authToken', info.token);
      await AsyncStorage.setItem('postData', JSON.stringify(postData));
    } catch (e) {
      throw e;
    }
  }

  async getListItems() {
    try {
      const data = await getItems();
      runInAction(() => {
        this.events = data;
      });
    } catch (e) {
      throw e;
    }
  }
}

export default new Store();
