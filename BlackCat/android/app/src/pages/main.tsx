import React, { FC } from 'react';
import { View } from 'react-native';
import NavBar from '../components/nav-bar';
import List from '../components/list';

interface Props {
  navigation: any;
}

const Main: FC<Props> = ({ navigation }) => {
  return (
    <View>
      <NavBar navigation={navigation}></NavBar>
      <List />
    </View>
  );
};

export default Main;
