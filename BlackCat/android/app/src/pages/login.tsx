import React, { FC, useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableHighlight,
  KeyboardAvoidingView,
} from 'react-native';
import styles from '../styles/login';
import userStore from '../stores/index';
import CatSvg from '../assets/SVGs/logo-cat.svg';
// import UserSvg from '../assets/SVGs/user.svg';
import AsyncStorage from '@react-native-async-storage/async-storage';
interface Props {
  navigation: any;
}

const Login: FC<Props> = ({ navigation }) => {
  const { logIn } = userStore;

  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  //TODO:自动登录实现方法有待商榷
  useEffect(() => {
    (async () => {
      try {
        const token = await AsyncStorage.getItem('authToken');
        if (token) {
          await logIn(
            JSON.parse((await AsyncStorage.getItem('postData')) ?? ''),
          );
          navigation.navigate('Main');
        }
      } catch (e) {}
    })();
  }, []);

  const handleSignIn = async () => {
    try {
      await logIn({ username, password });
      navigation.navigate('Main');
    } catch (e) {
      console.log('密码错误！');
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../assets/Image/Street-Dance-01.jpg')}
        style={styles.image}>
        <View style={styles.main}>
          <View style={styles.slogan}>
            <Text style={{ color: '#D5EF7F', fontSize: 16, fontWeight: '500' }}>
              FIND THE MOST LOVED ACTIVITIES
            </Text>
          </View>
          <View style={styles.BC}>
            <Text style={{ color: '#D5EF7F', fontSize: 24, fontWeight: '700' }}>
              BLACK CAT
            </Text>
          </View>
          {/* TODO: SVG展示 */}
          <View style={styles.icon}>
            <CatSvg width={45} height={45} fill="#D5EF7F"></CatSvg>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <TextInput
              style={[styles.input, styles.username]}
              placeholder="Username"
              placeholderTextColor="#AC8EC9"
              //TODO: inlineImageLeft=""
              value={username}
              onChangeText={(text: string) => setUsername(text)}
            />
            <TextInput
              style={[styles.input, styles.password]}
              placeholder="Password"
              placeholderTextColor="#AC8EC9"
              value={password}
              onChangeText={(text: string) => setPassword(text)}
            />
          </KeyboardAvoidingView>
        </View>
      </ImageBackground>
      <TouchableHighlight onPress={handleSignIn}>
        <View style={styles.signBtn}>
          <Text style={styles.btnText}>SIGN IN</Text>
        </View>
      </TouchableHighlight>
    </View>
  );
};

export default Login;
