import React, { FC, useEffect } from 'react';
import {
  View,
  Text,
  FlatList,
  VirtualizedList,
  SafeAreaView,
  Image,
} from 'react-native';
import { useObserver } from 'mobx-react';
import Store from '../../stores';
import styles from './style';
import { geneTime } from '../../utils/functions';

interface Props {}

interface ItemProps {
  info: any;
}

const List: FC<Props> = () => {
  const { events, getListItems } = Store;

  const MainCard: FC<ItemProps> = ({ info }) => (
    <>
      <View>
        <Text style={styles.title}>{info.name}</Text>
      </View>
      <View style={{ height: 15, marginTop: 8 }}>
        {/* TODO: SVG */}
        <Text style={styles.time}>{`${geneTime(info.begin_time)} - ${geneTime(
          info.end_time,
        )}`}</Text>
      </View>
      <View style={{ marginTop: 12 }}>
        <Text style={styles.content}>{info.description}</Text>
      </View>
      <View style={styles.activities}>
        <View>
          {/* TODO: SVG */}
          <Text style={styles.going}>
            {info.is_going ? 'I am going!' : `${info.goings_count ?? 0} Going`}
          </Text>
        </View>
        <View style={{ marginLeft: 30 }}>
          {/* TODO: SVG */}
          <Text style={styles.going}>
            {info.is_liked ? 'I like it' : `${info.likes_count ?? 0} Likes`}
          </Text>
        </View>
      </View>
    </>
  );

  const Item: FC<ItemProps> = ({ info }) => {
    return (
      <View style={styles.item}>
        <View style={styles.userInfo}>
          <View style={styles.user}>
            <Image
              style={styles.avatar}
              source={{
                uri: info.creator?.avatar,
              }}
            />
            <Text style={styles.name}>{info.creator?.username}</Text>
          </View>
          <View style={styles.channel}>
            <Text>{info.channel?.name}</Text>
          </View>
        </View>
        <View style={info.images?.length > 0 && styles.mainInfo}>
          {info.images?.length ? (
            <View style={styles.twoBars}>
              <View style={{ flex: 1 }}>
                <MainCard info={info}></MainCard>
              </View>
              <View style={{ width: 64 }}>
                {info.images?.map((uri: string) => (
                  <Image
                    source={{ uri }}
                    width={64}
                    height={64}
                    style={{ marginBottom: 8 }}
                  />
                ))}
              </View>
            </View>
          ) : (
            <MainCard info={info}></MainCard>
          )}
        </View>
      </View>
    );
  };

  const renderItem = ({ item }) => <Item info={item} />;

  useEffect(() => {
    getListItems();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={events?.events}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};

export default List;
