import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 16,
    // paddingRight: 16,
    flex: 1,
    backgroundColor: 'yellow',
  },
  item: {
    height: 208,
    backgroundColor: 'skyblue',
  },
  userInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  user: {
    height: 20,
  },
  avatar: {
    height: 20,
    width: 20,
    // borderRadius
  },
  name: {
    fontSize: 12,
    color: '#67616D',
    lineHeight: 20,
    marginLeft: 8,
  },
  channel: {
    height: '100%',
    borderWidth: 1,
    borderColor: '#D3C1E5',
    borderRadius: 10,
  },
  mainInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
    color: '#453257',
  },
  time: {
    fontSize: 12,
    color: '#8560A9',
    lineHeight: 15,
  },
  content: {},
  activities: {
    height: 15,
    marginTop: 12,
  },
  going: {
    fontSize: 12,
    color: '#453257',
    marginLeft: 5,
  },
  twoBars: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
