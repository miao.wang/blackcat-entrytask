import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  main: {
    height: 40,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#8560A9',
    paddingLeft: 14,
    paddingRight: 14,
  },
  leftBtn: {
    height: 24,
    width: 24,
    backgroundColor: 'white',
  },
  cat: {
    height: 24,
    width: 24,
    backgroundColor: 'white',
  },
  rightBtn: {
    height: 24,
    width: 24,
    backgroundColor: 'white',
  },
});
