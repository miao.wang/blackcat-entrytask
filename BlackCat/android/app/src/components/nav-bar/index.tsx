import React, { FC } from 'react';
import { View, TouchableHighlight } from 'react-native';
import styles from './style';

interface Props {
  navigation: any;
}

const NavBar: FC<Props> = ({ navigation }) => {
  const { routeName } = navigation?.state;

  const handleClickLeft = () => {
    navigation.navigate(routeName === 'Main' ? 'Search' : 'Main');
  };
  const handleClickRight = () => {
    navigation.navigate('Me');
  };

  return (
    <View style={styles.main}>
      <TouchableHighlight style={styles.leftBtn} onPress={handleClickLeft}>
        <View></View>
      </TouchableHighlight>
      <View style={styles.cat}></View>
      <TouchableHighlight style={styles.rightBtn} onPress={handleClickRight}>
        <View></View>
      </TouchableHighlight>
    </View>
  );
};

export default NavBar;
