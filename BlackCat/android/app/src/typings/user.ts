export interface UserInfo {
  token: string;
  user: User;
}

export interface User {
  avator: string;
  email: string;
  goings_count: string;
  id: number;
  likes_count: number;
  past_count: number;
  userName: string;
}

export interface PostData {
  username: string;
  password: string;
}

export interface Events {
  events: any[];
  has_more: boolean;
}
